	/*  Wizard */
	jQuery(function ($) {
		"use strict";
		var data = {};
		var products = {
			"1a two wing windows" : 0.73,
			"1b fixed window" : 0.25,
			"5b-windows" : 0.39,
			"window_1" : 0.44,
			"window_14" : 0.56,
			"window_17" : 0.20,
			"window_44" : 0.35
		}
		var customer = JSON.parse(localStorage.getItem('customer'))
		var form = $('form')
		$('.another-product').hide();
		$('form#wrapped').attr('action', 'form_send_without_branch.php');
		if (customer) {
			form.find("input[name=first_name]").val(customer.first_name)
			form.find("input[name=last_name]").val(customer.last_name)
			form.find("input[name=email]").val(customer.email)
			form.find("input[name=phone]").val(customer.phone)
		}
		var wizzie = $("#wizard_container").wizard({
			stepsWrapper: "#wrapped",
			submit: ".submit",
			beforeSelect: function (event, state) {
				if ($('input#website').val().length != 0) {
					return false;
				}
				if (state.isMovingForward) {
					var inputs = $(this).wizard('state').step.find(':input',':select',':radio');
					inputs.each(function(){
						data[$(this).attr('name')] = $(this).val()
					})
					if (state.stepIndex >= 5) {
						summary()
						$('.another-product').show()
					} else {
						$('.another-product').hide()
					}
					if (state.stepIndex == 6) {
						updateProducts()
						allWindows()
					}
					return !inputs.length || !!inputs.valid();

				} else {
					return true;					
				}
			}
		}).validate({
			errorPlacement: function (error, element) {
				if (element.is(':radio') || element.is(':checkbox')) {
					error.insertBefore(element.next());
				} else {
					error.insertAfter(element);
				}
			}
		});

		$('.forward').click(summary)

		$('.another-product').click(function () {
			updateProducts()
			$('form').trigger("reset");
			location.reload()
		})

		function updateProducts() {
			let stored_products = JSON.parse(localStorage.getItem('order_items'))
			if (!stored_products)
				stored_products = {};
			stored_products[Date.now()] = data
			localStorage.setItem('order_items', JSON.stringify(stored_products));
		}

		function summary() {
			const profit = .3
			data['width'] = $('form').find("input[name=width]").val()
			data['height'] = $('form').find("input[name=height]").val()
			let full_name = data['first_name'] + ' ' + data['last_name']
			let dimensions = data['width'] + ' x ' + data['height'] + 'cm'
			let area = data['width'] * data['height']
			let price = (products[data['product']] * area * (1 + profit))
			if (data['profile'] == 'Decco-70mm')
				price = price * .75
			data['price'] = price.toFixed(2)
			$("#smr-name").text(full_name)
			$("#smr-email").text(data['email'])
			$("#smr-phone").text(data['phone'])
			$("#smr-type").text(data['product_type'])
			$("#smr-product").text(data['product'])
			$("#smr-dimensions").text(dimensions)
			$("#smr-unit-price").text(products[data['product']] + ' Kč')
			$("#smr-area").html(area + 'cm<sup>2</sup>')
			$("#smr-price").text(money(price))
			$("#smr-price-with-inst").text(money(price*1.15))
			$("#smr-price-wout-inst").text(money(price*1.21))
			if (data['first_name']) {
				localStorage.setItem('customer', JSON.stringify({
					'first_name': data['first_name'],
					'last_name': data['last_name'],
					'email': data['email'],
					'phone': data['phone']
				}));
			}
		}

		function allWindows() {
			let orderItems = JSON.parse(localStorage.getItem('order_items'));
			for(const item in orderItems){
				let newId = 'order_item_' + item
				let product = orderItems[item]
				if ($('#' + newId).length)
					break
				let domItem = $('#order-item-1').clone().attr('id', newId).insertBefore('#order-items-list div:first')
				domItem.find('strong').text(orderItems[item].window_name)
				domItem.find('.smr-dimensions').text(product.width + ' x ' + product.height + 'cm')
				domItem.find('.smr-price').text(money(product.price))
			}
		}

		function money(amount) {
			if (isNaN(amount))
				amount = 0
			const formatter = new Intl.NumberFormat('cz',{
  				style: 'currency',
				currency: 'CZK',
			  	signDisplay: 'never',
			});
			let currency = formatter.format(amount) + ' Kč'
			return currency.substr(4)
		}
	});